const esbuild = require("esbuild");
const htmlmin = require("html-minifier");

const input = "src";
const output = "_site";

const serve = process.env.ELEVENTY_RUN_MODE === "serve";

module.exports = eleventyConfig => {
	eleventyConfig.setLayoutResolution(false);

	eleventyConfig.on(
		"eleventy.after",
		async ({ dir, results, runMode, outputMode }) => {
			await Promise.all([
				esbuild.build({
					entryPoints: ["src/style.css", "src/interface.ts", "src/embed.ts"],
					outdir: output,
					format: "esm",
					bundle: true,
					splitting: true,
					minify: !serve,
					sourcemap: true,
					//write: serve,
				}),
				esbuild.build({
					entryPoints: ["src/audio-worklet/audio-worklet.ts"],
					outdir: output,
					format: "esm",
					bundle: true,
					splitting: false,
					minify: !serve,
					sourcemap: true,
					//write: serve,
				}),
			]);
		},
	);

	eleventyConfig.addTransform("minify html", function (content) {
		if (this.page.outputPath?.endsWith(".html")) {
			return htmlmin.minify(content, {
				useShortDoctype: true,
				removeComments: true,
				collapseWhitespace: true,
			});
		}
		return content;
	});
	//config.setServerPassthroughCopyBehavior("passthrough"); // doesn't work
	eleventyConfig.addPassthroughCopy({ assets: "/" });
	eleventyConfig.addWatchTarget("src/**/*.css");
	eleventyConfig.addWatchTarget("src/**/*.js");
	eleventyConfig.addWatchTarget("src/**/*.ts");
	eleventyConfig.addWatchTarget("src/**/*.json");
	return {
		dir: {
			input,
			output,
		},
	};
};
