export default {
	roots: ["./src"],
	preset: "ts-jest",
	testEnvironment: "jsdom",
};
