smallmode is a method to store a bytebeat "mode" (bytebeat, floatbeat, funcbeat) in a single byte.
the tiny data is great for encoding into a small url or file.
it allows a ton of things and has plenty of unused areas in case you want to add more features.

no player has all of these features yet but i considered everything that seemed reasonable to implement.
i will not change anything already defined in the format because i already made my urls and backwards compatibility should stay forever.

some pseudo javascript to show how the format works:

```js
mode & 0x80 ? /* code is statements */ : /* code is an expression */
mode & 0x40 ? /* returns a function */ : /* returns a number */
mode & 0x20 ? /* time as seconds */ : /* time as sample number */
mode & 0x10 ? /* clip out of range samples */ : /* wrap out of range samples */

const num = mode & 0x0f;
if (!num) {
	// unused, would be 1 bit signed but that's a pretty stupid mode
	// 0x00 is an invalid mode and will NEVER be standardized
	// if you want an unusual custom mode, i highly suggest using 0x00 and putting some extra data after it
} else if (num < 11) {
	// integer

	2**(num >> 1) // bit depth of output
	mode & 0x01 ? /* unsigned output */ : /* signed output */
	// when unsigned, the output is in the range [0, 2**2**(num >> 1))
	// when signed, the output is in the range [-(2**2**(num >> 1)), 2**2**(num >> 1) / 2)
} else if (num === 11) {
	// unused, would be 32 bit unsigned but javascript doesn't support that for normal numbers
} else if (num < 13) {
	// float

	mode & 0x01 ? /* from 0.0 to 1.0 */ : /* from -1.0 to 1.0 */
} else {
	// unused
}
```
