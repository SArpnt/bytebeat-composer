import { initTextarea } from "./textarea.ts";

// must be constructed after dom is loaded
export class CodeEditor {
	getCode: () => string;
	setCode: (value?: string) => void;

	constructor(editCallback: () => void) {
		const textarea = document.getElementById("code-editor") as HTMLTextAreaElement;
		initTextarea(editCallback, textarea);
		this.getCode = () => textarea.value;
		this.setCode = v => {
			textarea.value = v as string;
		};

		// codemirror takes a long time to load, so import it late, then replace the textarea
		import("./codemirror.ts").then(c => {
			const codemirror = c.replace(editCallback, textarea);
			this.getCode = () => codemirror.state.doc.toString();
			this.setCode = v =>
				// codemirror type is wrong
				// TODO pull request fix
				// @ts-expect-error
				codemirror.dispatch({
					changes: { from: 0, to: codemirror.state.doc.length, insert: v },
				});
		});
	}
}
