export default {
	main: document.getElementsByTagName("main")[0],
	error: document.getElementById("error")!,
	canvas: document.getElementById("canvas-main") as HTMLCanvasElement, // this is the only shared element
	timeCursor: document.getElementById("canvas-timecursor")!,

	timeUnit: document.getElementById("control-time-unit")!,
	timeUnitLabel: document.getElementById("control-time-unit-label")!,
	timeValue: document.getElementById("control-time-value") as HTMLInputElement,

	scaleUp: document.getElementById("control-scaleup") as HTMLButtonElement,
	scaleDown: document.getElementById("control-scaledown") as HTMLButtonElement,

	playbackMode: document.getElementById("control-song-mode") as HTMLSelectElement,
	sampleRate: document.getElementById("control-sample-rate") as HTMLInputElement,
	volume: document.getElementById("control-volume") as HTMLInputElement,

	canvasTogglePlay: document.getElementById("canvas-toggleplay")!,

	controls: document.getElementById("controls")!,
	canvasContainer: document.getElementById("canvas-container")!,
};
