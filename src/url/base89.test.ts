import { into89, from89 } from "./base89.ts";

test("all 1 from start", () => {
	const data = new Uint8Array(Array(10).fill(255));
	for (let i = 100; i; i--) {
		const inout = from89(into89(data, i));

		const bits = Math.max(0, data.length * 8 - i);
		const len = bits >> 3;
		const rem = bits & 7;
		for (let j = 0; j < len; j++) {
			expect(inout[j]).toBe(255);
		}
		expect(inout[len] | 0).toBe((1 << rem) - 1);
		for (let j = inout.length; j > len; j--) {
			expect(inout[j] | 0).toBe(0);
		}
	}
});

test("all 1 from end", () => {
	const data = new Uint8Array(Array(10).fill(255));
	for (let i = 100; i; i--) {
		const inout = from89(into89(data, i), i);

		const bits = Math.min(i, data.length * 8);
		const len = bits >> 3;
		const rem = bits & 7;
		for (let j = 0; j < len; j++) {
			expect(inout[j]).toBe(0);
		}
		if (len < data.length) {
			expect(inout[len]).toBe((255 << rem) & 255);
		}
		for (let j = len + 1; j < data.length; j++) {
			expect(inout[j]).toBe(255);
		}
	}
});

// TODO random data tests
