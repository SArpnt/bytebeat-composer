// TODO everything here is extremely poorly optimized
// into and from iterate over bits at every step, they should go over full bytes at some point
// should probably benchmark the website

const tochar =
	"!#$&'()*+,-./0123456789:;=?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_abcdefghijklmnopqrstuvwxyz{|}~";
const fromchar = Object.fromEntries(Object.entries(tochar).map(([i, v]) => [v, BigInt(i)]));

// go through 123 bits at a time, into 19 character chunks
// bitIndex from 0 to 7 to trim bits off the input data

/** least amount of bits that requires a base 89 string of a specific length. */
//const lbl = Array(20)
//	.fill(undefined)
//	.map((_, i) => i && (89n ** BigInt(i - 1)).toString(2).length);

/**
 * converts data into base89, using all the valid characters for a url anchor.
 * this allows incredibly small urls, but many of these characters
 * break markdown and other common post formatting.
 *
 * bitIndex is the first bit that into89 will read from and encode,
 * and it'll continue until the end of the array.
 * use bitIndex if you want to trim less than 8 bits off the data.
 */
export function into89(data: Uint8Array, bitIndex = 0): string {
	let coded = "";
	while (true) {
		//const byteIndex = bitIndex >> 3;
		//const bitOffset = bitIndex & 7;
		// collects 123 bits of data
		let chunkInt = 0n;
		for (let i = 0; i < 123; i++) {
			if (bitIndex >> 3 < data.length) {
				// if bitIndex is negative, this will implicitly cast
				const currentByte = data[bitIndex >> 3] as number;
				const bitOfByte = 1 << (bitIndex & 7);
				chunkInt += BigInt(!!(currentByte & bitOfByte)) << BigInt(i);
				bitIndex++;
			} else {
				// length kept being wrong, i gave up on it

				//bigint = BigInt.asUintN(123, bigint << BigInt(bitOffset));
				//for (const bits of lbl.slice(1).map(x => x - 1)) {
				//	if (bits >= i) {
				//		return coded;
				//	}
				//	coded += tochar[Number(bigint % 89n)];
				//	bigint /= 89n;
				//}
				//// unreachable
				while (chunkInt) {
					coded += tochar[Number(chunkInt % 89n)];
					chunkInt /= 89n;
				}
				return coded;
			}
		}
		//bigint = BigInt.asUintN(123, bigint << BigInt(bitOffset));
		for (let j = 0; j < 19; j++) {
			coded += tochar[Number(chunkInt % 89n)];
			chunkInt /= 89n;
		}
	}
}

/**
 * converts data from base89, the opposite of into89.
 * the decoded data will be placed at bitIndex in the returned array.
 * the length will always be equal or longer than the original data,
 * and any extra data will be 0 bits. this is fine for a deflate stream,
 * which already encodes the end in the format.
 */
export function from89(coded: string, bitIndex = 0 /*, byteUp = true*/): Uint8Array {
	// all this code is PROBABLY correct but i'm not sure because into89 length is wrong anyways

	//const chunkCount = Math.max(Math.ceil(coded.length / 19) - 1, 0);
	//const chunkRem = coded.length - chunkCount * 19;
	//const lbla = lbl[chunkRem];
	//const bitrem = byteUp ? (lbla + 7 & ~7) - (bitIndex & 7) : lbla;
	//const bits = chunkCount * 123 + bitrem;
	const bits = bitIndex + Math.ceil(coded.length / 19) * 123; // always too long but that's good for now
	const data = new Uint8Array((bits + 7) >> 3);

	let currentByte = 0;
	if (coded.length) {
		const chunks = coded.match(/.{1,19}/g) as RegExpMatchArray;
		for (const chunk of chunks) {
			let chunkInt = 0n;
			for (const letter of chunk.split("").reverse()) {
				chunkInt *= 89n;
				chunkInt += fromchar[letter];
			}
			for (let i = 123; i; i--) {
				const bit = Number(chunkInt & 1n);
				currentByte |= bit << (bitIndex & 7);
				if ((bitIndex & 7) === 7) {
					data[bitIndex >> 3] = currentByte;
					currentByte = 0;
				}
				bitIndex++;
				if (bitIndex === bits) {
					if ((bitIndex & 7) !== 0) {
						data[bitIndex >> 3] = currentByte;
					}
					return data;
				}
				chunkInt >>= 1n;
			}
		}
	}

	return data;
}
