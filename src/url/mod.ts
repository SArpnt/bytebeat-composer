import { deflateSync, inflateSync } from "fflate";
import { from89 } from "./base89.ts";
import type { Song, SongMode } from "../common.ts";

/** @see smallmode.md */
const smallmode_table = {
	Bytebeat: 0b00000111,
	"Signed Bytebeat": 0b00000110,
	Floatbeat: 0b00011100,
	Funcbeat: 0b11111100,
} as const;

// handmade, not very effective. not using it right now
//const deflateDictionary5 = new TextEncoder().encode(
//	Object.getOwnPropertyNames(Math).join("(t*") +
//		Object.getOwnPropertyNames(Math).join("(t+") +
//		Object.getOwnPropertyNames(Math).join("(PI*") +
//		Object.getOwnPropertyNames(Math).join("(t*=") +
//		"int(t/=)**int(t//t--t++t**t&&!(t||t]??=0,\n\n]||=.00x=x=>(),(+=this.globalThis.window.5*random()*=\"[t>>='[t>>=`[t>>t,\".charCodeAt(t,'.charCodeAt(t,`.charCodeAt(t;i++);i++);return Math.alse-(function(){for(i=0;i<(a,b)=>(",
//);

export async function fromUrlData(hash: string): Promise<Song | undefined> {
	const v = hash[1];

	try {
		if (v === "6" || v === "5") {
			// #6, #5

			let data: Uint8Array;
			let compressedCode;
			if (v === "6") {
				data = from89(hash.substring(2), 12);
				// if data[1] is undefined the implicit cast to 0 is fine
				data[1] = (data[1] >> 4) | 0x40;
				compressedCode = data.subarray(6);
			} else {
				// add "00" to shift over float bytes and set first 4 bits to 0100
				data = Uint8Array.from(
					atob(`00${hash.substring(2, 8)}`),
					// @ts-ignore
					c => c.charCodeAt(),
				);
				compressedCode = Uint8Array.from(
					atob(hash.substring(8)),
					// @ts-ignore
					c => c.charCodeAt(),
				);
			}

			// this may be useful for extra properties without a new url version
			/*
			if (rateMode[1] >= 76) {
				// number >= 2**25
			}
			*/

			const dataview = new DataView(data.buffer);
			const sampleRate = dataview.getFloat32(1);
			if (sampleRate >= 2 ** 24) {
				console.error("Couldn't load data from url: invalid samplerate");
				return;
			}

			const maybe_mode = Object.entries(smallmode_table).find(x => x[1] === data[5]);
			if (maybe_mode === undefined) {
				console.error("Couldn't load data from url: invalid mode");
				return;
			}
			const mode = maybe_mode[0] as SongMode;

			const code = new TextDecoder().decode(
				inflateSync(
					compressedCode,
					// a dictionary can be added later without breaking compatibility
				),
			);

			return { code, sampleRate, mode };
		}
		if (v === "v") {
			// #v4, #v3b64
			const dataString = atob(hash.substring(hash[2] === "4" ? 3 : 6));

			// @ts-ignore
			const dataBuffer = Uint8Array.from(
				dataString,
				// @ts-ignore
				c => c.charCodeAt(),
			);

			const songDataString = new TextDecoder().decode(inflateSync(dataBuffer));
			let { code, sampleRate, mode } = JSON.parse(songDataString);

			code += "";
			sampleRate = Math.fround(sampleRate ?? 8000);
			mode = mode ?? "Bytebeat";
			if (sampleRate < 2 || sampleRate >= 2 ** 24) {
				console.error("Couldn't load data from url: invalid samplerate");
				return;
			}
			// @ts-ignore
			if (smallmode_table[mode] === undefined) {
				console.error("Couldn't load data from url: invalid mode");
				return;
			}

			return { code, sampleRate, mode };
		}
	} catch (err) {
		console.error("Couldn't load data from url:", err);
		return;
	}
	console.error("Unrecognized url data");
}

//version 6 breaks markdown and probably other things
/*
async function setUrlData({ code, mode, sampleRate }: Song) {
	const smallmode = smallmode_table[mode];

	const compressedCode = deflateSync(new TextEncoder().encode(code), {
		level: 8,
	});

	const buffer = new ArrayBuffer(5 + compressedCode.length);
	const dataview = new DataView(buffer);
	dataview.setFloat32(0, sampleRate);
	dataview.setUint8(4, smallmode);

	const dataArray = new Uint8Array(buffer);
	dataArray.set(compressedCode, 5);
	dataArray[0] <<= 4;

	const base89 = into89(dataArray, 4);

	window.location.hash = `#6${base89}`;
}*/

/**
 * sampleRate MUST be a 32 bit float within the range [2, 16777215].
 * other samplerates in range will automatically round.
 * samplerates out of the range won't get stored correctly at all.
 * you can do Math.min(Math.max(Math.fround(sampleRate), 2), 2 ** 24 - 1)
 * to get any number within the correct range.
 *
 * this function sets the window hash directly, it doesn't return a string.
 */
export async function setUrlData({ code, mode, sampleRate }: Song) {
	const smallmode = smallmode_table[mode];

	const compressedCode = deflateSync(new TextEncoder().encode(code), {
		level: 8,
	});

	const buffer = new ArrayBuffer(6 + compressedCode.length);
	const dataview = new DataView(buffer);
	dataview.setFloat32(1, sampleRate);
	dataview.setUint8(5, smallmode);

	const dataArray = new Uint8Array(buffer);
	dataArray.set(compressedCode, 6);

	const dataString = dataArray.reduce((a, b) => a + String.fromCharCode(b), "");
	const base64 = btoa(dataString).substring(2).replaceAll("=", "");

	window.location.hash = `#5${base64}`;
}
